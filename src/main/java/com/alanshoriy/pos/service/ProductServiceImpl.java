package com.alanshoriy.pos.service;

import com.alanshoriy.pos.dto.ProductDto;
import com.alanshoriy.pos.entity.Product;
import com.alanshoriy.pos.mapper.ProductMapper;
import com.alanshoriy.pos.repository.ProductRepository;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Muttaqin Alanshoriy
 */
@Service
public class ProductServiceImpl implements ProductService{

    @Autowired
    private ProductRepository productRepository;
    
    @Override
    public List<ProductDto> getProducts() {
        List<Product> products = productRepository.findAll();
        List<ProductDto> productDtos = products.stream()
                .map((product) -> (ProductMapper.mapToProductDto(product)))
                .collect(Collectors.toList());
        return productDtos;
    }

    @Override
    public ProductDto getProduct(Long productId) {
        Optional<Product> product = productRepository.findById(productId);
        if (product.isPresent()) {
            return ProductMapper.mapToProductDto(product.get());
        } else {
            return null;
        }
    }

    @Override
    public void updateProduct(ProductDto productDto) {
        Product product = ProductMapper.mapToProduct(productDto);
        productRepository.save(product);
    }

    @Override
    public void deleteProduct(Long productId) {
        productRepository.deleteById(productId);
    }

    @Override
    public void saveProduct(ProductDto productDto) {
        Product product = ProductMapper.mapToProduct(productDto);
        productRepository.save(product);
    }
    
}
