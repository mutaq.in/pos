package com.alanshoriy.pos.service;

import com.alanshoriy.pos.dto.ProductDto;
import java.util.List;

/**
 *
 * @author user
 */
public interface ProductService {
    public List<ProductDto> getProducts();
    public ProductDto getProduct(Long productId);
    public void updateProduct(ProductDto productDto);
    public void deleteProduct(Long productId);
    public void saveProduct(ProductDto productDto);
}
