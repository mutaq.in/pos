package com.alanshoriy.pos.controller;

import com.alanshoriy.pos.dto.ProductDto;
import com.alanshoriy.pos.service.ProductService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.view.RedirectView;

/**
 *
 * @author Muttaqin Alanshoriy
 */
@Controller
public class ProductController {
    @Autowired
    private ProductService productService;
    
    @GetMapping("/products")
    public String list(Model model){
        List<ProductDto> products = productService.getProducts();
        model.addAttribute("products", products);
        return "product/index";
    }
    
    @GetMapping("/products/{productId}")
    public String view(Model model, @PathVariable Long productId){
        ProductDto product = productService.getProduct(productId);
        model.addAttribute("product", product);
        return "product/view";
    }
    
    @GetMapping("/products/create")
    public String create(Model model){
        model.addAttribute("product", new ProductDto());
        return "product/create";
    }
    
    @GetMapping("/products/{productId}/edit")
    public String edit(Model model, @PathVariable Long productId) {
        ProductDto product = productService.getProduct(productId);
        if (product == null) {
            return "/product";
        }
        model.addAttribute("product", product);
        return "/product/edit";
    }
    @RequestMapping(value = "/products", method = RequestMethod.POST)
    public RedirectView store(Model model, @ModelAttribute("product") ProductDto productDto) {
        model.addAttribute("product", productDto);
        productService.saveProduct(productDto);
        return new RedirectView("/products");
    }
    @RequestMapping(value = "/products/{productId}/delete", method = RequestMethod.POST)
    public RedirectView delete(@PathVariable Long productId) {
        productService.deleteProduct(productId);
        return new RedirectView("/products");
    }
}
