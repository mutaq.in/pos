package com.alanshoriy.pos.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

/**
 *
 * @author Muttaqin Alanshoriy
 */
@Controller
public class HomeController {
    @GetMapping("/")
    public String index(){
        return "dashboard";
    }
}
