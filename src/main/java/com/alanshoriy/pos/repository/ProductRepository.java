package com.alanshoriy.pos.repository;

import com.alanshoriy.pos.entity.Product;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author user
 */
public interface ProductRepository extends JpaRepository<Product, Long>{
    
}
